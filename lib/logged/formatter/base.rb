# frozen_string_literal: true

module Logged
  module Formatter
    # Definiation of a logged formatter
    class Base
      def call(_data); end
    end
  end
end

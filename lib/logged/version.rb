# frozen_string_literal: true

module Logged
  # Version
  VERSION = '0.3.0'
end

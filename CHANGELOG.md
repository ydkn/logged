# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

- Support for Rails 7.0

### Changed

- Droped support for Rails 4.x

## [0.2.0] - 2019-08-22

### Added

- Support for Rails 6.0

### Changed

- Add magic comments for frozen string literals to all files
- Update rubocop settings
- Use rubocop-performance and rubocop-rails plugins

## [0.1.2] - 2017-04-29

### Changed

- Rails 5.1 compatibility

## [0.1.1] - 2016-09-16

### Changed

- Rails 5.0 compatibility

## [0.1.0] - 2015-08-13

### Changed

- Allow to disable/enable logger
